import { Request, Response } from "express";
import { quotes } from "./db";
import { Sequelize } from "sequelize";

export const getRandomQuote = async (_: Request, res: Response) => {
    // Select a random quote from the database
    const q = await quotes.findOne({
        order: Sequelize.literal('RANDOM()')
    });

    res.json(q);
}

export const getRandomQuoteProxied = async (_: Request, res: Response) => {
    res.redirect(301, 'https://api.quotable.io/quotes/random')
}