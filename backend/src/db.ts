import { Sequelize, DataTypes } from 'sequelize';

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './test.db'
});

export const quotes = sequelize.define('quotes', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    content: DataTypes.TEXT,
    createdAt: DataTypes.INTEGER,
    updatedAt: DataTypes.INTEGER,
});