import express from 'express';
import cors from 'cors';
import { getRandomQuote, getRandomQuoteProxied } from './quote';

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static("public"));

// Simple health endpoint to assert the API is running correctly
app.get('/health', (_, res) => {
    res.send('zpour');
})

// Simple route to random quote
app.get('/quotes/random', getRandomQuote);
app.get('/quotes/random_proxied', getRandomQuoteProxied);

app.listen(3000, async () => {
    console.log('Server is running on port 3000');
})