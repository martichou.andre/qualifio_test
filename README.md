## Why?

I did not understand why or how you wanted me to use the public quotes API:
- using it as a simple proxy??
- without writing a backend??

That's why I went with sqlite with local database containing quotes.
I've also included a endpoint that simply redirect the request (proxied).

## How?

You have a Dockerfile and a docker-compose files available.

```
docker-compose up
```

The resulting API will run on port `3000`.

## Possible improvements

### App
- use OpenAPI for REST API declaration
- use express routes (if the API expands, no need for now with 1 endpoint)

### Database
- do not use sqlite
- if we have to handle a lot of reads and some writes: multiple PG instances and replicas

## Notes

- I'm available to discuss the decisions I made as it may not be what you were expecting.
- React code is pretty bad, first time using it.